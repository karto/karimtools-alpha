# Karim Tools (alpha)

Project to wrangling data at low-simple-custom scale

## Data Encryption

Small issue: Old database, we can't delegate work to DB.
Managed to extract a bunch of JSON files (exporter next one to add)

### Prerequisites

1. Maven: https://maven.apache.org/
Life is easier when you use maven for Java projects

### Installing


```
mvn verify
mvn clean install
mvn compile
```

## Running 

### JsonCrypt

```
mvn exec:java -Dexec.mainClass="com.falabella.app.JsonCrypt" -Dexec.args="file.json.bz2"
```

### CsvCrypt

Remember to escape the " " " strings

```
mvn exec:java -Dexec.mainClass="com.falabella.app.CsvCrypt" -Dexec.args="\"filepath/hola.csv.gz\" \"CSV_SEPARATOR\" \"COLUMN1_TO_ENCRYPT,COLUMN_TO_ENCRYPT\"" 
```


## Built With

* [Commons](https://commons.apache.org/) - Bunch of tools everybody should use
* [Maven](https://maven.apache.org/) - Dependency Management
* [Spring Security](https://spring.io/projects/spring-security) - Used to generate encryption

## Contributing

Still no use

## Authors

* **Karim Touma** - *Initial work* - [karto](https://github.com/karto)


## License

This project is licensed under the Karim License 

## Acknowledgments

* To no idea who
 

