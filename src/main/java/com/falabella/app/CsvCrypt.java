package com.falabella.app;

import java.io.*;
import java.util.*;
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.crypto.keygen.KeyGenerators;

//mvn exec:java -Dexec.mainClass="com.falabella.app.CsvCrypt"
// -Dexec.args="/Users/karintouma/Desktop/test_ft_bol_venta/FT_BOL_VENTA_20181113.json.bz2"

public class CsvCrypt {

    public static void main( String[] args ){

        final String password = "macoy123";  
        final String salt = "08c92cfa3745bcdb";

        		try {       

 	       		//String pathOrigin = "/Users/karintouma/Google Drive/work/code/karimtools-alpha/test.csv.gz";
        		//String delimeter = ",";
        		//String columnsToEncrypt = "1,3";
        		// Obviously we should refine this part
        		String pathOrigin = args[0];
        		String delimeter = args[1];
        		String columnsToEncrypt = args[2];

        		System.out.println(pathOrigin);
        		String[] columnsToEncryptArray = columnsToEncrypt.split(",");

        		FileInputStream fin = new FileInputStream(pathOrigin);
				BufferedInputStream bis = new BufferedInputStream(fin);
				CompressorInputStream input = new CompressorStreamFactory().createCompressorInputStream(bis);
				BufferedReader br2 = new BufferedReader(new InputStreamReader(input));

				String line = "";
				// This should be fixed, i guess
				OutputStream fout = new FileOutputStream(pathOrigin.replace(".csv.gz","_enc.csv.gz"));
				OutputStream gzip = new GzipCompressorOutputStream(fout);
				TextEncryptor encryptor = Encryptors.queryableText(password, salt);      

				while ((line = br2.readLine()) != null) {


					// System.out.println(line);
					String[] tokens = line.split(delimeter);
					// Iterate through columns
					for (int i=0;i<tokens.length;i++) {

						if(tokens[i] != null && !tokens[i].isEmpty() && Arrays.asList(columnsToEncryptArray).contains(String.valueOf(i)))
							tokens[i]=encryptor.encrypt(tokens[i]);
					
					}

				    // Adding the EOL, may vary on different OS
                    // TODO: Make EOL platform independent
					String lineOut = String.join(",", tokens)+"\n";
					gzip.write(lineOut.getBytes());


				}

				System.out.println(pathOrigin.replace(".csv.gz","_enc.csv.gz"));

				gzip.close();
				br2.close();
    			input.close();
    			bis.close();
    			fin.close();

        		} catch (Exception e) {
		            e.printStackTrace();
		        }

    }


}