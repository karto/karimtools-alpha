package com.falabella.app;

import java.io.*;
import java.util.*;
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.crypto.keygen.KeyGenerators;
 
public class JsonCrypt {

    public static void main( String[] args )
    {
    	
    	Gson gson = new GsonBuilder().create();
    	// place store this guys in another place, or secure another way
        final String password = "PASSWORD";  
        final String salt = "__16bytesrandom_";
        //final String salt = KeyGenerators.string().generateKey();

		try {       

                    // TODO: This should be validated
                    // This is made to encrypt JSON bziped files
                    // Path of the file by args
                    String pathOrigin = args[0];

					FileInputStream fin = new FileInputStream(pathOrigin);
				    BufferedInputStream bis = new BufferedInputStream(fin);
    				CompressorInputStream input = new CompressorStreamFactory().createCompressorInputStream(bis);
    				BufferedReader br2 = new BufferedReader(new InputStreamReader(input));
                    // TODO: This should be validated
			 		// replacing .json.bz2 --> _enc.json.bz2
                    // this renaming is crap, please do something better here
					OutputStream fout = new FileOutputStream(pathOrigin.replace(".json.bz2","_enc.json.bz2"));
					OutputStream bzip2 = new BZip2CompressorOutputStream(fout);

					String line = "";
			 		int rowNum = 0;

    				while ((line = br2.readLine()) != null) {
    					rowNum++;
    						
    					//System.out.println(line);
    					// Loading JSON to Class using gson
    					Entity entity = gson.fromJson(line, Entity.class);
    					// Creating encryptor /with fixed password and salt
				        TextEncryptor encryptor = Encryptors.queryableText(password, salt);      

				        // this should be a setter method, only encrypt if is not null
				        // and obviously we should create an iterator

        				if (entity.Column1 != null && !entity.Column1.isEmpty())
        					entity.Column1 = encryptor.encrypt(entity.Column1);
   						

        				// Could reuse encryptor but wanted to show reconstructing TextEncryptor
				        // TextEncryptor decryptor = Encryptors.queryableText(password, salt);
				        // String decryptedText = decryptor.decrypt(encryptedText);

				        // Adding the EOL, may vary on different OS
                        // TODO: Make EOL platform independent
				        String jsonInString = gson.toJson(entity)+"\n";
				        //System.out.println(jsonInString);
				        bzip2.write(jsonInString.getBytes());
    				}

                    // Closing everything
    				bzip2.close();
					br2.close();
    				input.close();
    				bis.close();
    				fin.close();

		        } catch (Exception e) {
		            e.printStackTrace();
		        }


    }
}
